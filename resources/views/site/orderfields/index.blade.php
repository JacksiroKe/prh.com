@extends('layouts.app', ['activePage' => 'orderfield-management', 'menuParent' => 'laravel', 'titlePage' => __('OrderField Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">recent_actors</i>
                </div>
                <h4 class="card-title">{{ __('Order Fields') }}</h4>
              </div>
              <div class="card-body">
                @can('create', App\Models\Site\OrderField::class)
                  <div class="row">
                    <div class="col-12 text-right">
                      <a href="{{ route('orderfields.create') }}" class="btn btn-sm btn-rose">{{ __('Add An Order Field') }}</a>
                    </div>
                  </div>
                @endcan
                <div class="table-responsive">
                  <table id="datatables" class="table table-striped table-no-bordered table-hover" style="display:none">
                    <thead class="text-primary">
                      <td>
                        {{ __('Label') }}
                      </td>
                      <td>
                        {{ __('Instruction') }}
                      </td>
                      <td>
                        {{ __('Type') }}
                      </td>
                      <td>
                        {{ __('Options') }}
                      </td>
                      <td>
                        {{ __('Attributes') }}
                      </td>
                      <td>
                        {{ __('Default Value') }}
                      </td>
                      <td>
                        {{ __('Position') }}
                      </td>
                      <td>
                        {{ __('Enabled') }}
                      </td>
                      <th>
                        {{ __('Creation date') }}
                      </th>
                      @can('manage-orderfields', App\Models\User::class)
                        <th class="text-right">
                          {{ __('Actions') }}
                        </th>
                      @endcan
                    </thead>
                    <tbody>
                      @foreach($orderfields as $orderfield)
                        <tr>
                          <td>
                            {{ $orderfield->label }}
                          </td>
                          <td>
                            {{ $orderfield->instruction }}
                          </td>
                          <td>
                            {{ $orderfield->type }}
                          </td>
                          <td>
                            {{ $orderfield->options }}
                          </td>
                          <td>
                            {{ $orderfield->attributes }}
                          </td>
                          <td>
                            {{ $orderfield->default_value }}
                          </td>
                          <td>
                            {{ $orderfield->position }}
                          </td>
                          <td>
                            {{ $orderfield->enabled }}
                          </td>
                          <td>
                            {{ $orderfield->created_at->format('Y-m-d') }}
                          </td>
                          @can('manage-orderfields', App\Models\User::class)
                            <td class="td-actions text-right">
                              @can('update', $orderfield)
                                <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('orderfields.edit', $orderfield) }}" data-original-title="" title="">
                                  <i class="material-icons">edit</i>
                                  <div class="ripple-container"></div>
                                </a>
                                <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('orderfields.destroy', $orderfield) }}" data-original-title="" title="">
                                  <i class="material-icons">delete</i>
                                  <div class="ripple-container"></div>
                                </a>
                              @endcan
                            </td>
                          @endcan
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
<script>
  $(document).ready(function() {
    $('#datatables').fadeIn(1100);
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search Order Fields",
      },
      "columnDefs": [
        { "orderable": false, "targets": 3 },
      ],
    });
  });
</script>
@endpush