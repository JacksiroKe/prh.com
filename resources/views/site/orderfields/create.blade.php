@extends('layouts.app', ['activePage' => 'orderfield-management', 'menuParent' => 'laravel', 'titlePage' => __('Order Field Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('orderfields.store') }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">recent_actors</i>
                </div>
                <h4 class="card-title">{{ __('Add an Order Field') }}</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('orderfields.index') }}" class="btn btn-sm btn-rose">{{ __('Back to list') }}</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Field Label') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('label') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('label') ? ' is-invalid' : '' }}" name="label" id="input-label" type="text" placeholder="{{ __('Field Label') }}" value="{{ old('label') }}" required="true" aria-required="true"/>
                      @include('alerts.feedback', ['field' => 'label'])
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Instructions') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('instruction') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('instruction') ? ' is-invalid' : '' }}" name="instruction" id="input-instruction" type="text" placeholder="{{ __('Instructions on the Form') }}" value="{{ old('instruction') }}">
                      @include('alerts.feedback', ['field' => 'instruction'])
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Field Type') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('type') ? ' has-danger' : '' }}">
                      <select class="selectpicker col-sm-12 pl-0 pr-0" name="type" data-style="select-with-transition" title="" data-size="100">
                        <option value="text" selected>{{ __('Single Line Text') }}</option>
                        <option value="number">{{ __('Number Input') }}</option>
                        <option value="textarea">{{ __('MultiLine Text') }}</option>
                        <option value="checkbox">{{ __('Checkbox Options') }}</option>
                        <option value="select">{{ __('Select Box Options') }}</option>
                        <option value="select-radio">{{ __('Radio Button') }}</option>
                      </select>
                      @include('alerts.feedback', ['field' => 'type'])
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Field Options') }}<br>{{ __('Optional') }}</label>
                  <div class="col-sm-5">
                    <div class="form-group{{ $errors->has('options') ? ' has-danger' : '' }}">
                      <textarea cols="30" rows="5" class="form-control{{ $errors->has('options') ? ' is-invalid' : '' }}" name="options" id="input-options" type="text" placeholder="{{ __('Options') }}">{{ old('options') }}</textarea>
                      @include('alerts.feedback', ['field' => 'options'])
                    </div>
                  </div>
                  <div class="col-sm-5">
                    For Checkbox, Select or Radio have each option on its own line.<br>For Example:<br><br>Label1#value1<br>Label2#value2
                  </div>                 
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Default Value') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('default_value') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('default_value') ? ' is-invalid' : '' }}" name="default_value" id="input-default_value" type="text" placeholder="{{ __('Default Value') }}" value="{{ old('default_value') }}">
                      @include('alerts.feedback', ['field' => 'default_value'])
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Attributes') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('attributes') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('attributes') ? ' is-invalid' : '' }}" name="attributes" id="input-attributes" type="text" placeholder="{{ __('Instructions on the Form') }}" value="{{ old('instruction') }}">
                      @include('alerts.feedback', ['field' => 'attributes'])
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Field Position') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('position') ? ' has-danger' : '' }}">
                      <select class="selectpicker col-sm-12 pl-0 pr-0" name="position" data-style="select-with-transition" title="" data-size="100">
                        <option value="1">{{ __('Position 1') }}</option>
                        <option value="2">{{ __('Position 2') }}</option>
                        <option value="3">{{ __('Position 3') }}</option>
                        <option value="4">{{ __('Position 4') }}</option>
                        <option value="5">{{ __('Position 5') }}</option>
                      </select>
                      @include('alerts.feedback', ['field' => 'position'])
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Field Status') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('enabled') ? ' has-danger' : '' }}">
                      <select class="selectpicker col-sm-12 pl-0 pr-0" name="enabled" data-style="select-with-transition" title="" data-size="100">
                        <option value="1">{{ __('Enabled') }}</option>
                        <option value="0">{{ __('Disabled') }}</option>
                      </select>
                      @include('alerts.feedback', ['field' => 'enabled'])
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-rose">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection