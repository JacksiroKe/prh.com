@extends('layouts.app', ['activePage' => 'page-management', 'menuParent' => 'laravel', 'titlePage' => __('Page Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('orderfields.store') }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">recent_actors</i>
                </div>
                <h4 class="card-title">{{ __('Add Page') }}</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('orderfields.index') }}" class="btn btn-sm btn-rose">{{ __('Back to list') }}</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-10">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <label>{{ __('Name of the Page') }}</label>
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" value="{{ old('name') }}" required="true" aria-required="true"/>
                      @include('alerts.feedback', ['field' => 'name'])
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-10">
                    <div class="form-group{{ $errors->has('url') ? ' has-danger' : '' }}">
                    <label>{{ __('Url of the Page') }}</label>
                      <input class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}" name="url" id="input-url" type="text" value="{{ old('url') }}" required="true" aria-required="true"/>
                      @include('alerts.feedback', ['field' => 'url'])
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-10">
                    <div class="form-group{{ $errors->has('content') ? ' has-danger' : '' }}">
                    <label>{{ __('Content of the Page') }}</label>
                      <textarea cols="30" rows="20" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" name="content" id="input-content" type="text" required="true" aria-required="true">{{ old('description') }}</textarea>
                      @include('alerts.feedback', ['field' => 'content'])
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-10">
                    <div class="form-group{{ $errors->has('published') ? ' has-danger' : '' }}">
                    <label>{{ __('Publish the Page') }}</label>
                      <select class="selectpicker col-sm-12 pl-0 pr-0" name="published" data-style="select-with-transition" title="" data-size="100">
                        <option value="1">{{ __('Published') }}</option>
                        <option value="0">{{ __('UnPublished') }}</option>
                      </select>
                      @include('alerts.feedback', ['field' => 'published'])
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-rose">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection