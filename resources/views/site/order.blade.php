@extends('layouts.app', [
  'class' => 'off-canvas-sidebar',
  'classPage' => 'order-page',
  'activePage' => 'order',
  'title' => __('Place an Order'),
  'frontend' => true
])

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-9 ml-auto mr-auto mb-3 text-center">
        <h3>{{ __('Professional Research Help') }} </h3>

      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
        <form class="form" method="post" action="{{ route('ordernow') }}">
          @csrf
          @method('put')

          <div class="card card-login card-hidden">
            <div class="card-header card-header-primary text-center">
              <h4 class="card-title">PLACE YOUR ORDER</h4>
            </div>
            <div class="card-body ">
              <?php echo $html_fields ?>
            </div>
            <div class="card-footer justify-content-center">
              <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Proceed With Your Order') }}</button>
            </div>
          </div>
        </form>
        <div class="row">
          <div class="col-6">
              @if (Route::has('password.request'))
                  <a href="#" class="text-light">
                      <small>{{ __('How it Works') }}</small>
                  </a>
              @endif
          </div>
          <div class="col-6 text-right">
              <a href="#" class="text-light">
                  <small>{{ __('Our Policies') }}</small>
              </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
<script>
  $(document).ready(function() {
    md.checkFullPageBackgroundImage();
    setTimeout(function() {
      // after 1000 ms we add the class animated to the login/register card
      $('.card').removeClass('card-hidden');
    }, 700);
  });
</script>
@endpush
