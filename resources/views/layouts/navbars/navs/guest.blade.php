<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
  <div class="container">
    <div class="navbar-wrapper">
      <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('material') }}/img/logo.png" alt="HMA" style="margin-top:-15px;" /></a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="sr-only">Toggle navigation</span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
      <ul class="navbar-nav">
        <li class="nav-item{{ $activePage == 'deal_list' ? ' active' : '' }} ">
          <a href="{{ route('home') }}" class="nav-link">
            <i class="material-icons">home</i> {{ __('Home') }}
          </a>
        </li>
        @auth()
        <li class="nav-item">
          <a href="{{ route('dashboard') }}" class="nav-link">
            <i class="material-icons">dashboard</i> {{ __('Dashboard') }}
          </a>
        </li>
        @endauth
        <li class="nav-item{{ $activePage == 'publish' ? ' active' : '' }} ">
          <a href="{{ route('order') }}" class="nav-link">
            <i class="material-icons">publish</i> {{ __('Order Now') }}
          </a>
        </li>
        @if (!auth()->user())
        <li class="nav-item{{ $activePage == 'register' ? ' active' : '' }}">
          <a href="{{ route('register') }}" class="nav-link">
            <i class="material-icons">person_add</i> {{ __('Register') }}
          </a>
        </li>
        <li class="nav-item{{ $activePage == 'login' ? ' active' : '' }}">
          <a href="{{ route('login') }}" class="nav-link">
            <i class="material-icons">fingerprint</i> {{ __('Login') }}
          </a>
        </li>
        @endif
        @auth()
          <li class="nav-item">
              <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                  <i class="material-icons">directions_run</i>
                  <span>{{ __('Logout') }}</span>
              </a>
          </li>
        @endauth
      </ul>
    </div>
  </div>
</nav>
<style>
  .dropdown-menu {
    top: 80%;
    left: calc(100% - 300px);
  }

  .dropdown-item .dropdown:hover .dropdown-content {
    display:block !important;
  }
</style>
<script>
  $(document).ready(function() {
    $(".selectpicker").selectpicker();
  });
</script>