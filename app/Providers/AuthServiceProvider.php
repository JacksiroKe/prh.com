<?php

namespace App\Providers;

use App\Models\User;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('manage-site', [UserPolicy::class, 'manageSite']);
        Gate::define('manage-users', [UserPolicy::class, 'manageUsers']);
        Gate::define('manage-subjects', [UserPolicy::class, 'manageSubjects']);
        Gate::define('manage-widgets', [UserPolicy::class, 'manageWidgets']);
        Gate::define('manage-pages', [UserPolicy::class, 'managePages']);
        Gate::define('manage-orderfields', [UserPolicy::class, 'manageOrderFields']);
        Gate::define('manage-orders', [UserPolicy::class, 'manageOrders']);
        Gate::define('manage-tasks', [UserPolicy::class, 'manageTasks']);
        Gate::define('manage-projects', [UserPolicy::class, 'manageProjects']);
    }
}
