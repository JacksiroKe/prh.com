<?php

namespace App\Http\Controllers\Projects;

use App\Models\User;
use App\Models\Projects\Task;
use App\Http\Controllers\Controller;
use App\Http\Requests\TaskRequest;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Task::class);
    }

    /**
     * Display a listing of the tasks
     *
     * @param \App\Models\Task  $model
     * @return \Illuminate\View\View
     */
    public function index(Task $task)
    {
        $this->authorize('manage-tasks', User::class);

        return view('projects.tasks.index', ['tasks' => $task->all()]);
    }

    /**
     * Show the form for creating a new task
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('projects.tasks.create');
    }

    /**
     * Store a newly created task in storage
     *
     * @param  \App\Http\Requests\TaskRequest  $request
     * @param  \App\Models\Task  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TaskRequest $request, Task $task)
    {
        $task->create($request->all());

        return redirect()->route('projects.tasks.index')->withStatus(__('Task successfully created.'));
    }

    /**
     * Show the form for editing the specified task
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\View\View
     */
    public function edit(Task $task)
    {
        return view('projects.tasks.edit', compact('task'));
    }

    /**
     * Update the specified task in storage
     *
     * @param  \App\Http\Requests\TaskRequest  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TaskRequest $request, Task $task)
    {
        $task->update($request->all());

        return redirect()->route('projects.tasks.index')->withStatus(__('Task successfully updated.'));
    }
}
