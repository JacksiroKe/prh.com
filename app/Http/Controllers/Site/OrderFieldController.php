<?php

namespace App\Http\Controllers\Site;

use App\Models\User;
use App\Models\Site\OrderField;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderFieldRequest;

class OrderFieldController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(OrderField::class);
    }

    /**
     * Display a listing of the orderfields
     *
     * @param \App\Models\OrderField  $model
     * @return \Illuminate\View\View
     */
    public function index(OrderField $orderfields)
    {
        $this->authorize('manage-orderfields', User::class);

        return view('site.orderfields.index', ['orderfields' => $orderfields->all()]);
    }

    /**
     * Show the form for creating a new orderfield
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('site.orderfields.create');
    }

    /**
     * Store a newly created orderfield in storage
     *
     * @param  \App\Http\Requests\OrderFieldRequest  $request
     * @param  \App\Models\OrderField  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(OrderFieldRequest $request, OrderField $orderfield)
    {
        try {
            $lines = preg_split('/\r\n|\r|\n/', $request['options']);

            OrderField::create([
                'label' => $request['label'],
                'type' => $request['type'],
                'instruction' => isset($request['instruction']) ? $request['instruction'] : null,
                'attributes' => isset($request['attributes']) ? $request['attributes'] : null,
                'default_value' => isset($request['default_value']) ? $request['default_value'] : null,
                'options' => isset($request['options']) ? implode(' || ', $lines) : null,
                'position' => $request['position'],
                'enabled' => 1,
            ]);
            return redirect()->route('orderfields.index')->withStatus(__('Order Field successfully created.'));
        } catch (\Exception $exception) {
            return redirect()->back()->withError('Error while creating Order Field');
        }
    }

    /**
     * Show the form for editing the specified orderfield
     *
     * @param  \App\Models\OrderField  $orderfield
     * @return \Illuminate\View\View
     */
    public function edit(OrderField $orderfield)
    {
        return view('site.orderfields.edit', compact('orderfield'));
    }

    /**
     * Update the specified orderfield in storage
     *
     * @param  \App\Http\Requests\OrderFieldRequest  $request
     * @param  \App\Models\OrderField  $orderfield
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(OrderFieldRequest $request, OrderField $orderfield)
    {
        try {
            $lines = preg_split('/\r\n|\r|\n/', $request['options']);

            $orderfield->label = $request['label'];
            $orderfield->type = $request['type'];
            $orderfield->instruction = isset($request['instruction']) ? $request['instruction'] : '';
            $orderfield->attributes = isset($request['attributes']) ? $request['attributes'] : '';
            $orderfield->default_value = isset($request['default_value']) ? $request['default_value'] : '';
            $orderfield->options = isset($request['options']) ? implode(' || ', $lines) : '';
            $orderfield->position = $request['position'];
            $orderfield->enabled = 1;
            $orderfield->update();

            return redirect()->route('site.orderfields.index')->withStatus(__('Order Field successfully updated.'));
        } catch (\Exception $exception) {
            return redirect()->back()->withError('Error while updating Order Field');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderField  $apptime
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderField $orderfield)
    {
        $orderfield->delete();

        return redirect()->route('site.orderfields.index')->withStatus(__('Order Field deleted successfully.'));
    }
}
