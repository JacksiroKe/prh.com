<?php

namespace App\Http\Controllers\Site;

use App\Models\User;
use App\Models\Site\Widget;
use App\Http\Controllers\Controller;
use App\Http\Requests\WidgetRequest;

class WidgetController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Widget::class);
    }

    /**
     * Display a listing of the widgets
     *
     * @param \App\Models\Widget  $model
     * @return \Illuminate\View\View
     */
    public function index(Widget $widget)
    {
        $this->authorize('manage-widgets', User::class);

        return view('site.widgets.index', ['widgets' => $widget->all()]);
    }

    /**
     * Show the form for creating a new widget
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('site.widgets.create');
    }

    /**
     * Store a newly created widget in storage
     *
     * @param  \App\Http\Requests\WidgetRequest  $request
     * @param  \App\Models\Widget  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(WidgetRequest $request, Widget $widget)
    {
        $widget->create($request->all());

        return redirect()->route('site.widgets.index')->withStatus(__('Widget successfully created.'));
    }

    /**
     * Show the form for editing the specified widget
     *
     * @param  \App\Models\Widget  $widget
     * @return \Illuminate\View\View
     */
    public function edit(Widget $widget)
    {
        return view('site.widgets.edit', compact('widget'));
    }

    /**
     * Update the specified widget in storage
     *
     * @param  \App\Http\Requests\WidgetRequest  $request
     * @param  \App\Models\Widget  $widget
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(WidgetRequest $request, Widget $widget)
    {
        $widget->update($request->all());

        return redirect()->route('site.widgets.index')->withStatus(__('Widget successfully updated.'));
    }
}
