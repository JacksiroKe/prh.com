<?php

namespace App\Http\Controllers\Site;

use App\Models\User;
use App\Models\Site\Page;
use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;

class PageController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Page::class);
    }

    /**
     * Display a listing of the pages
     *
     * @param \App\Models\Page  $model
     * @return \Illuminate\View\View
     */
    public function index(Page $page)
    {
        $this->authorize('manage-pages', User::class);

        return view('site.pages.index', ['pages' => $page->all()]);
    }

    /**
     * Show the form for creating a new page
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('site.pages.create');
    }

    /**
     * Store a newly created page in storage
     *
     * @param  \App\Http\Requests\PageRequest  $request
     * @param  \App\Models\Page  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PageRequest $request, Page $page)
    {
        $page->create($request->all());

        return redirect()->route('pages.index')->withStatus(__('Page successfully created.'));
    }

    /**
     * Show the form for editing the specified page
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\View\View
     */
    public function edit(Page $page)
    {
        return view('site.pages.edit', compact('page'));
    }

    /**
     * Update the specified page in storage
     *
     * @param  \App\Http\Requests\PageRequest  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PageRequest $request, Page $page)
    {
        $page->update($request->all());

        return redirect()->route('site.pages.index')->withStatus(__('Page successfully updated.'));
    }
}
