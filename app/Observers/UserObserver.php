<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Facades\File;

class UserObserver
{
    /**
     * Handle the User "updating" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updating(User $user)
    {
        if ($user->avatar != $user->getOriginal('avatar')) {
            File::delete(storage_path("/app/public/{$user->getOriginal('avatar')}"));
        }
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        if ($user->avatar) {
            File::delete(storage_path("/app/public/{$user->avatar}"));
        }
    }
}
