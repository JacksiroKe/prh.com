<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can see the users.
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the authenticate user can create users.
     *
     * @param  \App\Models\User $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the authenticate user can update the user.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return boolean
     */
    public function update(User $user, User $model)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the authenticate user can delete the user.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return boolean
     */
    public function delete(User $user, User $model)    {
        return $user->isAdmin() && $user->id != $model->id;
    }

    /**
     * Determine whether the authenticate user can manage other users.
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function manageUsers(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the authenticate user can manage items and other related entities(tags, categories).
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function manageSite(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the authenticate user can manage items and other related entities(tags, subjects).
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function manageSubjects(User $user)
    {
        return $user->isAdmin() || $user->isWriter();
    }

    /**
     * Determine whether the authenticate user can manage items and other related entities(tags, widgets).
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function manageWidgets(User $user)
    {
        return $user->isAdmin() || $user->isWriter();
    }

    /**
     * Determine whether the authenticate user can manage items and other related entities(tags, pages).
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function manageOrderFields(User $user)
    {
        return $user->isAdmin() || $user->isWriter();
    }

    /**
     * Determine whether the authenticate user can manage items and other related entities(tags, pages).
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function managePages(User $user)
    {
        return $user->isAdmin() || $user->isWriter();
    }

    /**
     * Determine whether the authenticate user can manage items and other related entities(tags, projects).
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function manageTasks(User $user)
    {
        return $user->isAdmin() || $user->isWriter();
    }

    /**
     * Determine whether the authenticate user can manage items and other related entities(tags, projects).
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function manageOrders(User $user)
    {
        return $user->isAdmin() || $user->isWriter();
    }

    /**
     * Determine whether the authenticate user can manage items and other related entities(tags, projects).
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function manageProjects(User $user)
    {
        return $user->isAdmin() || $user->isWriter() || $user->isClient();
    }

}
